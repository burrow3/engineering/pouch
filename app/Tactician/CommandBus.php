<?php

declare(strict_types=1);

namespace App\Tactician;

use Illuminate\Contracts\Container\Container;
use App\Tactician\Middleware\AclCommandMiddleware;
use App\Tactician\Middleware\DryRunMiddleware;
use App\Tactician\Middleware\LogCommandMiddleware;
use App\Tactician\Middleware\ValidateCommandMiddleware;
use App\Domain\Core\Commands\Command;
use App\Domain\Core\Events\CommandQueuedEvent;
use App\Domain\Core\Events\DispatchesEvents;
use League\Tactician\CommandBus as TacticianBus;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\Handler\CommandNameExtractor\CommandNameExtractor;
use League\Tactician\Handler\Locator\HandlerLocator;
use League\Tactician\Handler\MethodNameInflector\MethodNameInflector;
use League\Tactician\Plugins\LockingMiddleware;

class CommandBus
{
    use DispatchesEvents;
    private Container $app;
    protected CommandNameExtractor $commandNameExtractor;
    protected MethodNameInflector $methodNameInflector;
    protected HandlerLocator $handleLocator;

    public function __construct(
        Container $app,
        MethodNameInflector $methodNameInflector,
        CommandNameExtractor $commandNameExtractor,
        HandlerLocator $handleLocator
    ) {
        $this->app = $app;
        $this->methodNameInflector = $methodNameInflector;
        $this->commandNameExtractor = $commandNameExtractor;
        $this->handleLocator = $handleLocator;
    }

    public function dispatch(Command $command, array $middleware = [])
    {
        $bus = new TacticianBus(
            $this->resolveMiddleware($middleware)
        );

        return $bus->handle($command);
    }

    public function queueDispatch(string $commandClass, array $commandParams)
    {
        // Run the command initially with a dry run
        $commandDry = new $commandClass(...$commandParams);
        $commandDry->dryRun();
        $this->dispatch($commandDry);

        // Dispatch an event to run the command at a later date
        $this->getDispatcher()->dispatch([
            new CommandQueuedEvent($commandClass, $commandParams),
        ]);
    }

    protected function resolveMiddleware(array $middleware): array
    {
        $m = [
            new LockingMiddleware(),
            $this->app->make(LogCommandMiddleware::class),
            $this->app->make(AclCommandMiddleware::class),
            $this->app->make(ValidateCommandMiddleware::class),
            $this->app->make(DryRunMiddleware::class),
        ];

        foreach ($middleware as $class) {
            $m[] = $this->app->make($class);
        }

        $m[] = new CommandHandlerMiddleware(
            $this->commandNameExtractor,
            $this->handleLocator,
            $this->methodNameInflector
        );

        return $m;
    }
}
