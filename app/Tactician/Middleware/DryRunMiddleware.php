<?php

declare(strict_types=1);

namespace App\Tactician\Middleware;

use League\Tactician\Middleware;

class DryRunMiddleware implements Middleware
{
    public function execute($command, callable $next): void
    {
        if ($command->isDryRun() === false) {
            $next($command);
        }
    }
}
