<?php

declare(strict_types=1);

namespace App\Tactician\Middleware;

use Exception;
use Illuminate\Contracts\Auth\Guard;
use App\Domain\Core\Commands\Command;
use League\Tactician\Middleware;

class AclCommandMiddleware implements Middleware
{
    /**
     * @var Guard
     */
    private $auth;

    /**
     * Create a new instance.
     *
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param Command $command
     * @param $next
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function execute($command, callable $next)
    {
        if (app()->runningInConsole() || empty($command->getACLs())) {
            return $next($command);
        }

        if (
            ($this->auth->guest() === false)
        ) {
            return $next($command);
        }

        throw new Exception('Insufficient permissions');
    }
}
