<?php

declare(strict_types=1);

namespace App\Tactician\Middleware;

use Exception;
use App\Domain\Core\Commands\Command;
use Illuminate\Support\Arr;
use League\Tactician\Middleware;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class LogCommandMiddleware implements Middleware
{
    /**
     * @var LoggerInterface
     */
    private $log;

    /**
     * @var Command
     */
    private $command;

    /**
     * @var string
     */
    private $uuid;

    /**
     * @var float
     */
    private $start;

    /**
     * @var array
     */
    private $exceptionToLogLevel = [];

    public function __construct(LoggerInterface $log)
    {
        $this->log = $log;
        $this->uuid = Uuid::uuid4()->toString();
    }

    /**
     * @param Command  $command
     * @param callable $next
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function execute($command, callable $next)
    {
        $this->command = $command;
        $this->start = microtime(true);
        $e = null;

        try {
            return $next($command);
        } catch (Exception $e) {
            throw $e;
        } finally {
            $this->logCommand($e);
        }
    }

    private function logCommand(Exception $e = null)
    {
        $end = microtime(true);
        $level = 'info';
        $details = [
            'tenant' => config('pouch.tenant', 'unknown'),
            'class' => \get_class($this->command),
            'dry-run' => $this->command->isDryRun(),
            'uuid' => $this->uuid,
            'start' => $this->start,
            'end' => $end,
            'duration' => $end - $this->start,
        ];

        if ($e instanceof Exception) {
            $level = $this->getExceptionLogLevel($e);

            $exceptionDetails = [
                'exception' => [
                    'class' => \get_class($e),
                    'message' => $e->getMessage(),
                    'code' => $e->getCode(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                ],
            ];

            $details += Arr::dot($exceptionDetails);
        }

        $this->log->log(
            $level,
            'Command executed',
            $details
        );
    }

    private function getExceptionLogLevel(Exception $e): string
    {
        $exceptionClass = \get_class($e);

        if (array_key_exists($exceptionClass, $this->exceptionToLogLevel)) {
            return $this->exceptionToLogLevel[$exceptionClass];
        }

        return 'debug';
    }
}
