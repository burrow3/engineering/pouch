<?php

declare(strict_types=1);

namespace App\Tactician\Middleware;

use Illuminate\Contracts\Container\Container;
use App\Domain\Core\Commands\Traits\ValidateCommandTrait;
use League\Tactician\Middleware;

class ValidateCommandMiddleware implements Middleware
{
    /**
     * @var Container
     */
    private $app;

    /**
     * @param Container $app
     */
    public function __construct(Container $app)
    {
        $this->app = $app;
    }

    public function execute($command, callable $next)
    {
        if ($this->commandNeedsValidating($command) && !$command->getValidationDisabled()) {
            $validator = $this->app->make($command->getValidator());
            $validator->validate($command->getValidationDetails());
        }

        return $next($command);
    }

    private function commandNeedsValidating($command)
    {
        return \in_array(
            ValidateCommandTrait::class,
            class_uses_recursive(\get_class($command)),
            true
        );
    }
}
