<?php

declare(strict_types=1);

namespace App\Tactician\Traits;

use DB;
use Exception;
use App\Tactician\CommandBus;
use App\Domain\Core\Commands\Command;

trait DispatchesCommands
{
    protected function queueDispatch(string $commandClass, array $commandParams)
    {
        return \app(CommandBus::class)->queueDispatch($commandClass, $commandParams);
    }

    protected function dispatch(Command $command)
    {
        return \app(CommandBus::class)->dispatch($command);
    }

    protected function dispatchAll(array $commands): array
    {
        $responses = [];

        DB::beginTransaction();

        try {
            foreach ($commands as $command) {
                $responses[] = $this->dispatch($command);
            }
        } catch (Exception $e) {
            DB::rollBack();

            throw $e;
        }

        DB::commit();

        return $responses;
    }

    protected function dryRunAll(array $commands): array
    {
        $responses = [];

        foreach ($commands as $command) {
            /** @var Command $command */
            $responses[] = $this->dryRun($command);
        }

        return $responses;
    }

    protected function dryRun(Command $command)
    {
        $dryRunCommand = clone $command;
        $dryRunCommand->dryRun();

        return \app(CommandBus::class)->dispatch($dryRunCommand);
    }
}
