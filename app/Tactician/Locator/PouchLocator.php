<?php

declare(strict_types=1);

namespace App\Tactician\Locator;

use Illuminate\Contracts\Container\Container;
use App\Domain\Core\Commands\CommandHandler;
use League\Tactician\Exception\MissingHandlerException;
use League\Tactician\Handler\Locator\HandlerLocator;

class PouchLocator implements HandlerLocator
{
    /**
     * @var Container
     */
    private $app;

    public function __construct(Container $app)
    {
        $this->app = $app;
    }

    /**
     * @param string $commandName
     *
     * @return CommandHandler
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function getHandlerForCommand($commandName): CommandHandler
    {
        $handler = str_replace(
            '\\Commands\\',
            '\\Handlers\\',
            $commandName
        );
        $handler .= 'Handler';

        if (!class_exists($handler)) {
            throw MissingHandlerException::forCommand($commandName);
        }

        return $this->app->make($handler);
    }
}
