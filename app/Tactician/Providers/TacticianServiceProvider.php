<?php

declare(strict_types=1);

namespace App\Tactician\Providers;

use Illuminate\Support\ServiceProvider;
use League\Tactician\Handler\CommandNameExtractor\CommandNameExtractor;
use League\Tactician\Handler\Locator\HandlerLocator;
use League\Tactician\Handler\MethodNameInflector\MethodNameInflector;

class TacticianServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(HandlerLocator::class, config('tactician.locator'));
        $this->app->bind(MethodNameInflector::class, config('tactician.inflector'));
        $this->app->bind(CommandNameExtractor::class, config('tactician.extractor'));
    }
}
