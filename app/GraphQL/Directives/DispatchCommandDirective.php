<?php

declare(strict_types=1);

namespace App\GraphQL\Directives;

use App\GraphQL\Services\DecamelTransformer;
use App\Tactician\Traits\DispatchesCommands;
use GraphQL\Language\AST\FieldDefinitionNode;
use GraphQL\Language\AST\NameNode;
use GraphQL\Language\AST\ObjectTypeDefinitionNode;
use GraphQL\Language\Parser;
use GraphQL\Type\Definition\ResolveInfo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use InvalidArgumentException;
use Nuwave\Lighthouse\Schema\AST\ASTHelper;
use Nuwave\Lighthouse\Schema\AST\DocumentAST;
use Nuwave\Lighthouse\Schema\Directives\BaseDirective;
use Nuwave\Lighthouse\Schema\Values\FieldValue;
use Nuwave\Lighthouse\Support\Contracts\FieldManipulator;
use Nuwave\Lighthouse\Support\Contracts\FieldResolver;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use Ramsey\Uuid\Uuid;
use ReflectionClass;

class DispatchCommandDirective extends BaseDirective implements FieldResolver, FieldManipulator
{
    use DispatchesCommands;

    public static function definition(): string
    {
        return /** @lang GraphQL */ <<<'SDL'
            """
            Resolve this field by dispatching a command using the user's input. This allows
            for creating basic CRUD mutations without needing any php code.

            If an 'input' field is provided on the input, then the user's input will use that,
            otherwise the general 'args' will be used instead.
            """
            directive @dispatchCommand(
              """
              Specify the class name of the command to dispatch.
              This directive will use reflection to determine how to convert the user input
              to pass to this command.

              If an 'id' is not present in the user's input, an 'id' will be generated to pass
              to this command. Ensure that your command supports 'id' and not something like
              'user_id' in order to use this.
              """
              command: String!

              """
              The transformer class to use for the user's input. This transformer can be a
              custom defined class or the default App\\GraphQL\\Services\\DecamelTransformer
              when not provided
              """
              transformer: String

              """
              If provided, then this is the id used for passing into the command and returning to the user
              the associated model. By default this is 'id', but some mutations will use more specific values
              like 'job_id'. Note that this is accessed AFTER the transformer is applied.
              """
              idField: String

              """
              If provided, then after the command is run the given model will attempt to be returned
              using the id for this mutation. This will be returned under a response type with a field
              that matches the name of the model. (e.g., EloquentUser will be returned as ['user' => <user model>]

              If not provided, then the ID for the model passed to this command will be used instead, and returned
              like ['id' => <id>] (or [<idField> => <id>] if idField is provided).
              """
              returnModel: String

              """
              An optional transformer class to use to modify the response. This can be useful if you return some
              metadata alongside with the command that you dispatch.

              This transformer should extend App\\GraphQL\\Services\\AbstractResponseTransformer.
              """
              responseTransformer: String

              """
              When true, an alternative mutation will ALSO be created in addition to the base
              mutation. This mutation (called <originalName>Queue) will support the same input
              as the base mutation but will return a `QueuedCommandResponse`.

              When fired, this mutation will first run the standard command validation, and then
              will dispatch an event to handle the mutation separate from this request.
              """
              canQueue: Boolean
            ) on FIELD_DEFINITION
            SDL;
    }

    public function manipulateFieldDefinition(
        DocumentAST &$documentAST,
        FieldDefinitionNode &$fieldDefinition,
        ObjectTypeDefinitionNode &$parentType
    ): void {
        $canQueue = $this->directiveArgValue('canQueue', false);

        if (!$canQueue) {
            return;
        }

        if ($parentType->name->value !== 'Mutation') {
            throw new InvalidArgumentException(
                'Failed to use @dispatchCommand directive: canQueue can only be true for mutations on the top level.'
            );
        }

        if (Str::endsWith($fieldDefinition->name->value, 'Queued')) {
            throw new InvalidArgumentException(
                'Failed to use @dispatchCommand directive: canQueue can\'t be used with mutations that end with the ' .
                'word Queued due to ambiguity.'
            );
        }

        $mutationType = $documentAST->types['Mutation'];

        /** @var FieldDefinitionNode $queuedFieldDefinition */
        $queuedFieldDefinition = $fieldDefinition->cloneDeep();
        $queuedFieldDefinition->name = new NameNode([
            'value' => $fieldDefinition->name->value . 'Queued',
        ]);

        $queuedFieldDefinition->type = Parser::parseType('QueuedMutationResponse', ['noLocation' => true]);

        $mutationType->fields = ASTHelper::mergeUniqueNodeList(
            $mutationType->fields,
            [
                $queuedFieldDefinition,
            ],
            true
        );
    }

    /**
     * Resolve the field directive.
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     *
     * @param  \Nuwave\Lighthouse\Schema\Values\FieldValue $fieldValue
     *
     * @return \Nuwave\Lighthouse\Schema\Values\FieldValue
     */
    public function resolveField(FieldValue $fieldValue): FieldValue
    {
        return $fieldValue->setResolver(
            function ($root, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): array {
                $commandClass = $this->directiveArgValue('command');
                $idField = $this->directiveArgValue('idField', 'id');

                $input = $this->getInput($args, $idField);
                $commandParams = $this->getCommandParams($input, $commandClass, $idField);

                if (Str::endsWith($resolveInfo->path[0], 'Queued')) {
                    $this->queueDispatch($commandClass, $commandParams);

                    return [
                        'id' => $input[$idField],
                        'success' => true,
                        'command' => $commandClass,
                    ];
                }

                $command = new $commandClass(...$commandParams);

                $this->dispatch($command);

                return $this->getResponse($input, $idField);
            }
        );
    }

    private function getInput(array $args, string $idField): array
    {
        $transformerClass = $this->directiveArgValue('transformer', DecamelTransformer::class);

        $input = \array_key_exists('input', $args) ? $args['input'] : $args;
        $transformer = \app($transformerClass);

        $treatedInput = $transformer->__invoke($input);

        if (!\array_key_exists($idField, $treatedInput)) {
            $treatedInput[$idField] = Uuid::uuid4()->toString();
        }

        return $treatedInput;
    }

    /**
     * Maps the transformed input from the user to the parameters for the command class.
     * This uses a few heuristics to try to avoid needing too much configuration.
     *
     * The way the logic works:
     *  - If the command takes a single array argument, just pass the input to that command
     *  - If it takes a single string, just pass the ID for this command
     *  - If it takes a string + array, pass [ID, input]
     *  - Otherwise, try to map the input to the param names - each of the params from
     *    the command will have their name converted to snake case and compared to the input.
     *    If all are present in the input, just pass them through.
     *
     * If none of the above works, an error is thrown to try to get the developer to set up
     * their command in a more helpful way.
     */
    private function getCommandParams(array $input, string $commandClass, string $idField): array
    {
        $reflection = new ReflectionClass($commandClass);
        $params = $reflection->getConstructor()->getParameters();

        if (\count($params) === 1) {
            if ($params[0]->getType()->getName() === 'array') {
                return [$input];
            }
            if ($params[0]->getType()->getName() === 'string') {
                return [$input[$idField]];
            }
        } elseif (
            \count($params) === 2 &&
            $params[0]->getType()->getName() === 'string' &&
            $params[1]->getType()->getName() === 'array'
        ) {
            return [$input[$idField], $input];
        } elseif (\count($params) >= 2) {
            $mappedParams = [];

            foreach ($params as $param) {
                $snakeParamName = Str::snake($param->getName());

                if (\array_key_exists($snakeParamName, $input)) {
                    $mappedParams[] = $input[$snakeParamName];
                }
            }

            if (\count($mappedParams) === \count($params)) {
                return $mappedParams;
            }
        }

        throw new InvalidArgumentException(
            'Failed to use @dispatchCommand directive: Couldn\'t figure out how to convert the user input' .
            ' to the params of the given command. Take a look at DispatchCommandDirective@getCommandParams to' .
            ' see how to map these params'
        );
    }

    private function getResponse(array $input, string $idField): array
    {
        $response = $this->getBaseResponse($input, $idField);
        $responseTransformerClass = $this->directiveArgValue('responseTransformer');

        if ($responseTransformerClass !== null) {
            $responseTransformer = \app($responseTransformerClass);

            $response = $responseTransformer->__invoke($input, $response);
        }

        return $response;
    }

    private function getBaseResponse(array $input, string $idField): array
    {
        $modelClass = $this->directiveArgValue('returnModel');

        // If modelClass is not provided, just return the ID (usually for delete commands)
        if ($modelClass === null) {
            return [
                $idField => $input[$idField],
            ];
        }

        // Otherwise, return the model that was created/affected

        /** @var Builder $query */
        $query = $modelClass::query();
        $model = $query->find($input[$idField]);
        $fieldName = $this->getResponseFieldName($modelClass);

        return [
            $fieldName => $model,
        ];
    }

    /**
     * Converts a full model class name to a string that can be used
     * in a field.
     *
     * e.g. <namepace>\EloquentUser becomes 'user'
     */
    private function getResponseFieldName(string $modelClass): string
    {
        $reflect = new ReflectionClass($modelClass);
        $className = \str_replace('Eloquent', '', $reflect->getShortName());

        return \lcfirst($className);
    }
}
