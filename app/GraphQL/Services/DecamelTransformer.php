<?php

declare(strict_types=1);

namespace App\GraphQL\Services;

use Carbon\Carbon;
use Illuminate\Support\Str;

class DecamelTransformer
{
    public function __invoke(array $input): array
    {
        return $this->treatArray($input);
    }

    /**
     * @param array $input
     *
     * @return array
     */
    public function treatArray(array $input): array
    {
        $treatedInput = [];

        foreach ($input as $key => $value) {
            if (\is_string($key)) {
                $key = Str::snake($key);
            }

            $treatedInput[$key] = $this->treatValue($value);
        }

        return $treatedInput;
    }

    /**
     * @param mixed $value
     *
     * @return mixed
     */
    protected function treatValue(mixed $value): mixed
    {
        if ($value instanceof Carbon) {
            return $value->format(Carbon::RFC3339);
        }

        if (\is_array($value)) {
            return $this->treatArray($value);
        }

        return $value;
    }
}
