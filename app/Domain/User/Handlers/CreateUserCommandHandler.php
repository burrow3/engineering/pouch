<?php

declare(strict_types=1);

namespace App\Domain\User\Handlers;

use Auth;
use DB;
use App\Domain\User\Commands\CreateUserCommand;
use App\Domain\User\Events\UserCreatedEvent;
use App\Domain\User\User;
use App\Domain\User\UserRepository;
use App\Domain\Core\Commands\CommandHandler;

class CreateUserCommandHandler extends CommandHandler
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly User $model,
    ) {
    }

    public function handle(CreateUserCommand $command): void
    {
        $this->details = $command->details;
        $this->details['id'] = $this->getEntityId();

        $this->model->id = $this->details['id'];

        $this->model->username = $this->details['username'];
        $this->model->email = $this->details['email'];
        $this->model->password = $this->details['password'];

        $user = Auth::user();

        DB::beginTransaction();

        $this->userRepository->save($this->model);

        DB::commit();

        $this
            ->getDispatcher()
            ->dispatch([
                new UserCreatedEvent($this->model->id),
            ]);
    }
}
