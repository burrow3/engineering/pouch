<?php

declare(strict_types=1);

namespace App\Domain\User\Handlers;

use DB;
use App\Domain\User\Commands\DeleteUserCommand;
use App\Domain\User\UserRepository;
use App\Domain\User\Events\UserDeletedEvent;
use App\Domain\Core\Commands\CommandHandler;

class DeleteUserCommandHandler extends CommandHandler
{
    public function __construct(
        private readonly UserRepository $userRepository,
    ) {
    }

    public function handle(DeleteUserCommand $command): void
    {
        DB::beginTransaction();

        try {
          $this->userRepository->delete($command->id);
        } catch (\Exception $e) {
          DB::rollBack();

          throw $e;
        }

        DB::commit();

        $this
            ->getDispatcher()
            ->dispatch([
                new UserDeletedEvent($command->id),
            ]);
    }
}
