<?php

declare(strict_types=1);

namespace App\Domain\User\Handlers;

use DB;
use App\Domain\User\Commands\UpdateUserCommand;
use App\Domain\User\User;
use App\Domain\User\UserRepository;
use App\Domain\User\Events\UserUpdatedEvent;
use App\Domain\Core\Commands\CommandHandler;

class UpdateUserCommandHandler extends CommandHandler
{
    public function __construct(
        private readonly UserRepository $userRepository,
    ) {
    }

    public function handle(UpdateUserCommand $command): void
    {
        $details = $command->details;
        /** @var User */
        $user = $this->userRepository->findOrFail($command->id);

        if (array_key_exists('username', $details)) {
          $user->username = $details['username'];
        }

        if (array_key_exists('password', $details)) {
          $user->password = $details['password'];
        }

        DB::beginTransaction();

        $this->userRepository->save($user);

        DB::commit();

        $this
            ->getDispatcher()
            ->dispatch([
                new UserUpdatedEvent($user->id),
            ]);
    }
}
