<?php

declare(strict_types=1);

namespace App\Domain\User\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use App\Domain\User\User;
use App\Domain\User\UserRepository;
use App\Domain\User\Events\UserEvent;
use App\Domain\Core\Exceptions\QueueEmailException;
use App\Domain\MessageSchedule\Services\ScheduleEmailService;

abstract class AbstractUserEventListener implements ShouldQueue
{
    private bool $accessCodeEnabled;
    protected User $user;

    public function __construct(
        private UserRepository $userRepository,
        private ScheduleEmailService $scheduleEmailService
    ) {
    }

    abstract public function getBodyTemplate(): string;

    abstract public function getSubjectTemplate(): string;

    abstract public function postEmailQueuedActions(array $sendToRecipientIds): void;

    public function handle(UserEvent $event): void
    {
        try {
            $userId = $event->userId;
            $this->user = $this->userRepository->findOrFail($userId);

            $recipients = $this->getRecipients();
            \Log::info('RRRR<<<<<');
            \Log::info(json_encode($recipients, JSON_PRETTY_PRINT));
            \Log::info('>>>>>RRRR');

            // $templateData = $this->getTemplateData();
            $bodyTemplate = $this->getBodyTemplate();
            $subjectTemplate = $this->getSubjectTemplate();

            $sendToRecipientIds = [];

            foreach ($recipients as $recipient) {
                $logRecipient = json_encode($recipient, JSON_PRETTY_PRINT);
                \Log::info('R<<<<<<<<<<');
                \Log::info($logRecipient);
                \Log::info('>>>>>>>>>>R');
                $primaryEmailAddress = $recipient->person->primaryEmailAddress();
                \Log::info('E<<<<<<<<<<');
                \Log::info($primaryEmailAddress);
                \Log::info('>>>>>>>>>>E');

                $accessCode = isset($recipient['access_code']) ? $recipient->access_code : '';
                \Log::info('A<<<<<<<<<<');
                \Log::info($accessCode);
                \Log::info('>>>>>>>>>>A');

                $templateDataWithRecipient = array_merge($this->getTemplateData($accessCode), ['recipient' => $recipient->person]);

                $this->scheduleEmailService
                    ->setBodyByTemplate($bodyTemplate, $templateDataWithRecipient)
                    ->setSubjectByTemplate($subjectTemplate, $templateDataWithRecipient)
                    ->setTo($primaryEmailAddress ? $primaryEmailAddress->email : null)
                    // ->setData([
                    //     'userName' => $this->user->name,
                    //     'recipientId' => $recipient['person']->id,
                    //     'url' => `/users/{$userId}/code/{$accessCode}`,
                    // ])
                    ->queueEmail();

                $sendToRecipientIds[] = $recipient->id;
            }

            $this->postEmailQueuedActions($sendToRecipientIds);
        } catch (QueueEmailException) {
            // Do nothing. The message schedule still got created

            return;
        } catch (\Exception $e) {
            \Log::error(
                get_class($event) . ' raised an unhandled exception',
                [
                    'exception' => $e,
                ]
            );
        }
    }

    private function getTemplateData(string $accessCode): array
    {
        return [
            'user' => $this->user,
            'extras' => $this->getExtras($accessCode),
        ];
    }

    private function getExtras(string $accessCode): array
    {
        $userId = $this->user->id;

        if (!$this->accessCodeEnabled) {
            return [
                'user_url' => \url('/') . "/spa/users/{$userId}/view",
            ];
        }

        if ($accessCode === '') {
            return [
                'user_url' => \url('/') . "/spa/users/{$userId}/view",
            ];
        }

        return [
            'user_url' => \url('/') . "/users/{$userId}/code/{$accessCode}",
        ];
    }
}
