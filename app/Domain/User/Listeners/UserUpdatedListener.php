<?php

declare(strict_types=1);

namespace App\Domain\User\Listeners;

use App\Domain\User\UserRepository;
use App\Domain\MessageSchedule\Services\ScheduleEmailService;

class UserUpdatedListener extends AbstractUserEventListener
{
    public function __construct(
        private UserRepository $userRepository,
        private ScheduleEmailService $scheduleEmailService
    ) {
        parent::__construct($userRepository, $scheduleEmailService);
    }

    public function postEmailQueuedActions(array $sentToRecipientIds): void
    {
    }

    public function getBodyTemplate(): string
    {
        return 'user.signature_request.body';
    }

    public function getSubjectTemplate(): string
    {
        return 'user.signature_request.subject';
    }
}
