<?php

declare(strict_types=1);

namespace App\Domain\User;

use Carbon\Carbon;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Support\Facades\Hash;
use App\Domain\Core\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property Carbon|null $email_verified_at
 * @property Carbon|null $last_logged_in_at
 * @property Carbon $created_at
 * @property Carbon|null $updated_at
 */

class User extends Model implements AuthenticatableContract
{

    use Authenticatable;

    public function __construct(array $attributes = [])
    {
        /*
            your model's $casts property provides a convenient method of converting attributes to common data types.
        */
        $this->casts = [
            'last_logged_in_at' => 'datetime',
            'password' => 'hashed',
        ];

        $this->hidden = [
            'password',
            'remember_token',
        ];

        $this->table = 'users';

        parent::__construct($attributes);
    }

    /**
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make(trim($password));
    }

    /**
     * @param string $username
     */
    public function setUsernameAttribute(string $username)
    {
        $this->attributes['username'] = strtolower($username);
    }

    public function updateLastLoggedInAt(): self
    {
        $this->last_logged_in_at = Carbon::now();
        $this->save();

        return $this;
    }

    public function save(array $options = []): bool
    {
        return parent::save($options);
    }
}
