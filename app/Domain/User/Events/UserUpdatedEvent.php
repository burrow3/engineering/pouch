<?php

declare(strict_types=1);

namespace App\Domain\User\Events;

class UserUpdatedEvent extends UserEvent
{
   /**
     * @var string
     */
    public $id;
}
