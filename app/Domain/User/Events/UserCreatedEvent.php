<?php

declare(strict_types=1);

namespace App\Domain\User\Events;

class UserCreatedEvent extends UserEvent
{
   /**
     * @var string
     */
    public $id;
}
