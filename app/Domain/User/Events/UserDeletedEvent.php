<?php

declare(strict_types=1);

namespace App\Domain\User\Events;

use App\Domain\User\Events;

class UserDeletedEvent extends UserEvent
{
   /**
     * @var string
     */
    public $id;
}
