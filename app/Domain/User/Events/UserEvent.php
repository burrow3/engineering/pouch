<?php

declare(strict_types=1);

namespace App\Domain\User\Events;

use App\Domain\Core\Events\Event;

abstract class UserEvent extends Event
{
    public function __construct(
        public string $userId
    ) {
    }
}
