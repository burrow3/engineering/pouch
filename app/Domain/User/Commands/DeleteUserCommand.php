<?php

namespace App\Domain\User\Commands;

use App\Domain\Core\Commands\Traits\ValidateCommandTrait;
use App\Domain\User\Validators\DeleteUserValidator;
use App\Domain\Core\Commands\Command;

class DeleteUserCommand extends Command
{
    use ValidateCommandTrait;

    public function __construct(
        public string $id
    ) {
    }

    public function getValidator(): string
    {
        return DeleteUserValidator::class;
    }
}
