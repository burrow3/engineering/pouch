<?php

namespace App\Domain\User\Commands;

use App\Domain\User\Validators\CreateUserValidator;
use App\Domain\Core\Commands\Command;
use App\Domain\Core\Commands\Traits\ValidateCommandTrait;

class CreateUserCommand extends Command
{
    use ValidateCommandTrait;

    public function __construct(
        array $details
    ) {
        $this->details = $details;
    }

    public function getValidator(): string
    {
        return CreateUserValidator::class;
    }
}
