<?php

namespace App\Domain\User\Commands;

use App\Domain\User\Validators\UpdateUserValidator;
use App\Domain\Core\Commands\Command;
use App\Domain\Core\Commands\Traits\ValidateCommandTrait;

class UpdateUserCommand extends Command
{
    use ValidateCommandTrait;

    public function __construct(
        public string $id,
        array $details
    ) {
        $this->details=$details;
    }

    public function getValidator(): string
    {
        return UpdateUserValidator::class;
    }
}
