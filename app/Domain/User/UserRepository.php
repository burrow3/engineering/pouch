<?php

declare(strict_types=1);

namespace App\Domain\User;

use App\Domain\Core\EloquentRepository;

class UserRepository extends EloquentRepository
{
    protected function model(): string
    {
        return User::class;
    }
}
