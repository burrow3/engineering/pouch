<?php

declare(strict_types=1);

namespace App\Domain\User\Validators;

use App\Domain\Core\Validators\AbstractValidator;

class DeleteUserValidator extends AbstractValidator
{
    protected function getRules(): array
    {
        return [
            'id' => [
                'required',
                'uuid',
                'exists:users,id,deleted_at,NULL',
            ],
        ];
    }

    protected function getMessages(): array
    {
        return [
            'id.required' => 'An user ID is required',
             'id.uuid' => 'An user ID must be an uuid',
            'id.exists' => 'An user ID must exist in user repository'
        ];
    }
}
