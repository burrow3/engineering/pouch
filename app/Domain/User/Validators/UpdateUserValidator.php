<?php

declare(strict_types=1);

namespace App\Domain\User\Validators;

use App\Domain\Core\Validators\AbstractValidator;

class UpdateUserValidator extends AbstractValidator
{
    protected function getRules(): array
    {
        return [
            'id' => [
                'required',
                'uuid',
                'exists:contract_templates,id,deleted_at,NULL',
            ],
            'username' => [
                'sometimes',
                'string',
            ],
            'password' => [
                'sometimes',
                'string',
            ],
        ];
    }

    protected function getMessages(): array
    {
        return [
            'id.required' => 'An user ID is required',
            'id.uuid' => 'An user ID must be an uuid',
            'id.exists' => 'An user ID must exist in user repository',

            'username.string' => 'The username must be a string',
            'password.string' => 'The password must be a string',
        ];
    }
}
