<?php

declare(strict_types=1);

namespace App\Domain\User\Validators;

use App\Domain\Core\Validators\AbstractValidator;

class CreateUserValidator extends AbstractValidator
{
    protected function getRules(): array
    {
        return [
            'username' => [
                'required',
                'string',
            ],
            'email' => [
                'required',
                'string',
            ],
            'password' => [
                'required',
                'string',
            ],
        ];
    }

    protected function getMessages(): array
    {
        return [
            'username.required' => 'A username is required',
            'username.string' => 'The username must be a string',
            'email.required' => 'An email is required',
            'email.string' => 'The email must be a string',
            'password.required' => 'A password is required',
            'password.string' => 'The password must be a string',
        ];
    }
}
