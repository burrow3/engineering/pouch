<?php

declare(strict_types=1);

namespace App\Domain\EventLog;

use Illuminate\Support\Collection;
use App\Domain\Core\EloquentRepository;

class EventLogRepository extends EloquentRepository
{
    protected function model()
    {
        return EloquentEventLog::class;
    }

    public function findByEventClassAndEventData(
        string $eventClass,
        array $eventData = [],
        array $select = ['*']
    ): Collection {
        return $this->findByEventNameAndEventData(
            \str_replace('\\', '.', $eventClass),
            $eventData,
            $select
        );
    }

    public function findByEventNameAndEventData(
        string $eventName,
        array $eventData = [],
        array $select = ['*']
    ): Collection {
        $eventLogs = $this->makeModel()
            ->where([
                'event_name' => $eventName,
            ]);

        if (!empty($eventData)) {
            $eventLogs->whereJsonContains('event', $eventData);
        }

        return $eventLogs->get($select);
    }
}
