<?php

declare(strict_types=1);

namespace App\Domain\EventLog;

use Carbon\Carbon;
use App\Domain\Core\Database\Eloquent\Model;

/**
 * @property string $id
 * @property string $event_name
 * @property array $event
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon|null $deleted_at
 */
class EloquentEventLog extends Model
{
    public function __construct(array $attributes = [])
    {
        $this->casts = [
            'event' => 'json',
        ];
        $this->revisionEnabled = false;
        $this->table = 'event_logs';

        parent::__construct($attributes);
    }
}
