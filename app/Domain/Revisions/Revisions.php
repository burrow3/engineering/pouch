<?php

declare(strict_types=1);

namespace App\Domain\Revisions;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Domain\Core\Database\Eloquent\Model;
use App\Domain\User\User;

/**
 * @property string $id
 * @property string $key
 * @property string $revisionable_type
 * @property string $revisionable_id
 * @property string $user_id
 * @property string|null $old_value
 * @property string|null $new_value
 * @property Carbon $created_at
 * @property EloquentUser $user
 */
class Revisions extends Model
{
    public function __construct(array $attributes = [])
    {
        $this->table = 'revisions';

        parent::__construct($attributes);
    }

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
