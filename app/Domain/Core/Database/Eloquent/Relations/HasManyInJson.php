<?php

declare(strict_types=1);

namespace App\Domain\Core\Database\Eloquent\Relations;

use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Domain\Core\Database\Eloquent\Model;

class HasManyInJson extends HasMany
{
    protected static $constraints = true;

    /**
     * @var string
     */
    private $json;

    /**
     * @var Closure|null
     */
    private $matchClosure = null;

    public function __construct(
        Builder $query,
        Model $parent,
        string $foreignKey,
        string $localKey,
        string $json,
        Closure $matchClosure = null
    ) {
        $this->json = $json;
        $this->matchClosure = $matchClosure;

        parent::__construct($query, $parent, $foreignKey, $localKey);
    }

    public function addConstraints()
    {
        $json = $this->json;

        if ($this->getParentKey() !== null) {
            $json = str_replace('?', $this->getParentKey(), $this->json);
        }

        $this->query->whereRaw($this->foreignKey . ' @> ' . $json);
    }

    public function addEagerConstraints(array $models)
    {
        $keys = $this->getKeys($models, $this->localKey);
        foreach ($keys as $key) {
            $this->query->orWhereRaw(
                $this->foreignKey . ' @> ' . str_replace('?', $key, $this->json)
            );
        }
    }

    public function match(array $models, Collection $results, $relation)
    {
        if ($this->matchClosure === null) {
            return $models;
        }

        foreach ($models as $model) {
            $filteredResults = new Collection();
            foreach ($results as $result) {
                if ($this->matchClosure->__invoke($model, $result)) {
                    $filteredResults[] = $result;
                }
            }
            $model->setRelation($relation, $filteredResults);
        }

        return $models;
    }
}
