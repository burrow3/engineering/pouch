<?php

declare(strict_types=1);

namespace App\Domain\Core\Database\Eloquent;

use Bosnadev\Database\Traits\UuidTrait;
use Closure;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Support\Str;
use App\Domain\Core\Database\Eloquent\Relations\BelongsTo;
use App\Domain\Core\Database\Eloquent\Relations\HasManyInJson;
use App\Domain\Core\Database\Eloquent\Relations\Pivot;

/**
 * @property string $id
 */
abstract class Model extends EloquentModel
{
    use UuidTrait;
    protected bool $revisionEnabled = true;
    protected bool $revisionCreationsEnabled = true;

    public function __construct(array $attributes = [])
    {
        $this->dateFormat = 'Y-m-d H:i:sO';
        $this->incrementing = false;
        $this->keyType = 'string';

        parent::__construct($attributes);
    }

    /**
     * @SuppressWarnings(PHPMD.StaticAccess)
     *
     * @param mixed      $table
     * @param mixed      $exists
     * @param null|mixed $using
     */
    public function newPivot(EloquentModel $parent, array $attributes, $table, $exists, $using = null)
    {
        return $using ? $using::fromRawAttributes($parent, $attributes, $table, $exists)
            : Pivot::fromAttributes($parent, $attributes, $table, $exists);
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @param string $related
     * @param string $foreignKey
     * @param string $otherKey
     * @param string $relation
     *
     * @return BelongsTo
     */
    public function belongsTo($related, $foreignKey = null, $otherKey = null, $relation = null)
    {
        // If no relation name was given, we will use this debug backtrace to extract
        // the calling method's name and use that as the relationship name as most
        // of the time this will be what we desire to use for the relationships.
        if ($relation === null) {
            [$current, $caller] = \debug_backtrace(0, 2);

            $relation = $caller['function'];
        }

        // If no foreign key was supplied, we can use a backtrace to guess the proper
        // foreign key name by using the name of the relationship function, which
        // when combined with an "_id" should conventionally match the columns.
        if ($foreignKey === null) {
            $foreignKey = Str::snake($relation) . '_id';
        }

        $instance = new $related();

        // Once we have the foreign key names, we'll just create a new Eloquent query
        // for the related models and returns the relationship instance which will
        // actually be responsible for retrieving and hydrating every relations.
        $query = $instance->newQuery();

        $otherKey = $otherKey ?: $instance->getKeyName();

        return new BelongsTo($query, $this, $foreignKey, $otherKey, $relation);
    }

    public function hasManyInJson(
        string $related,
        string $foreignKey = null,
        string $localKey = null,
        string $json = null,
        Closure $matchClosure = null
    ) {
        $foreignKey = $foreignKey ?: $this->getForeignKey();

        /** @var Model $instance */
        $instance = new $related();
        $localKey = $localKey ?: $this->getKeyName();

        return new HasManyInJson(
            $instance->newQuery(),
            $this,
            $instance->getTable() . '.' . $foreignKey,
            $localKey,
            $json,
            $matchClosure
        );
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @param string $related
     * @param string $foreignKey
     * @param string $otherKey
     * @param string $relation
     *
     * @return BelongsTo
     */
    public function belongsToWithoutScopes(
        $related,
        $foreignKey = null,
        $otherKey = null,
        $relation = null
    ): BelongsTo {
        // If no relation name was given, we will use this debug backtrace to extract
        // the calling method's name and use that as the relationship name as most
        // of the time this will be what we desire to use for the relationships.
        if ($relation === null) {
            [$current, $caller] = \debug_backtrace(0, 2);

            $relation = $caller['function'];
        }

        // If no foreign key was supplied, we can use a backtrace to guess the proper
        // foreign key name by using the name of the relationship function, which
        // when combined with an "_id" should conventionally match the columns.
        if ($foreignKey === null) {
            $foreignKey = Str::snake($relation) . '_id';
        }

        $instance = new $related();

        // Once we have the foreign key names, we'll just create a new Eloquent query
        // for the related models and returns the relationship instance which will
        // actually be responsible for retrieving and hydrating every relations.
        $query = $instance->newQueryWithoutScopes();

        $otherKey = $otherKey ?: $instance->getKeyName();

        return new BelongsTo($query, $this, $foreignKey, $otherKey, $relation);
    }

    /**
     * https://laravel.com/docs/7.x/upgrade#date-serialization
     */
    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format($this->dateFormat);
    }
}
