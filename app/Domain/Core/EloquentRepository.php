<?php

declare(strict_types=1);

namespace App\Domain\Core;

use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Container\Container;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Domain\Core\Database\Eloquent\Model;
use App\Domain\Core\Exceptions\RepositoryException;
use Ramsey\Uuid\Uuid;

/**
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
abstract class EloquentRepository implements Repository
{
    /**
     * @var DatabaseManager
     */
    protected $databaseManager;

    /**
     * @var array
     */
    protected $eagerLoad = [];

    /**
     * @var array
     */
    protected $filterable = [];

    /**
     * @var Builder
     */
    protected $model;

    /**
     * @var array
     */
    protected $orderBy = ['created_at' => 'desc'];

    public function __construct(
        protected Container $app
    ) {
        $this->databaseManager = $this->app->make(DatabaseManager::class);
    }

    /**
     * @param array $columns
     *
     * @return Collection
     *
     * @throws RepositoryException
     */
    public function all(array $columns = ['*'])
    {
        return $this->query()->get($columns);
    }

    public function create(array $data = [])
    {
        return $this->query()->create($data);
    }

    /**
     * @throws Exception
     */
    public function delete($id): ?bool
    {
        return $this->find($id)->delete();
    }

    public function deleteWhereIn(string $field, array $value): int
    {
        return $this->query()->whereIn($field, $value)->delete();
    }

    public function eagerLoad($relations): Repository
    {
        $relations = \is_array($relations) ? $relations : \func_get_args();

        $this->eagerLoad = \array_merge($this->eagerLoad, $relations);

        return $this;
    }

    /**
     * @return Collection|Model|null
     *
     * @throws RepositoryException
     */
    public function find(string $id, array $columns = ['*'])
    {
        $id = trim($id);

        if (!Uuid::isValid($id)) {
            return null;
        }

        return $this->query()->find($id, $columns);
    }

    /**
     * @return Collection|Model
     *
     * @throws RepositoryException
     */
    public function findOrFail(string $id, array $columns = ['*'])
    {
        $id = trim($id);

        if (!Uuid::isValid($id)) {
            throw (new ModelNotFoundException())
                ->setModel($this->model());
        }

        return $this->query()->findOrFail($id, $columns);
    }

    public function getFilterable(): array
    {
        return $this->filterable;
    }

    public function getOrderBy(): array
    {
        return $this->orderBy;
    }

    /**
     * @throws RepositoryException
     * @throws BindingResolutionException
     */
    public function makeModel(): Builder
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model) {
            throw new RepositoryException(
                "Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model"
            );
        }

        return $this->model = $model->newQuery()->with($this->eagerLoad);
    }

    /**
     * Make sure that direction is one of either 'ASC' or 'DESC'
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    public function orderBy($columns = []): Repository
    {
        if (\is_string($columns)) {
            $columns = [$columns => 'ASC'];
        }

        foreach ($columns as $column => &$direction) {
            $direction = \mb_strtoupper($direction);
            if (!\in_array($direction, ['ASC', 'DESC'], true)) {
                $direction = 'ASC';
            }
        }

        $this->orderBy = $columns;

        return $this;
    }

    public function save(Model $model): bool
    {
        return $model->save();
    }

    public function saveMinor(Model $model, array $options = []): bool
    {
        // Check to see if we have access to the saveMinor method and if so
        // use that
        if (\method_exists($model, 'saveMinor')) {
            return $model->saveMinor($options);
        }

        // If a model doesn't have the 'saveMinor' method then just go ahead
        // and do a normal save
        return $model->save($options);
    }

    public function update(string $id, array $data = []): void
    {
    }

    public function where($field, $operator, $value, $columns = ['*'])
    {
        return $this->query()->where($field, $operator, $value)->get($columns);
    }

    public function whereIn(string $field, array $values, array $columns = ['*']): Collection
    {
        return $this->query()->whereIn($field, $values)->get($columns) ?? new Collection();
    }

    public function with(array $array)
    {
        return $this->query()->with($array);
    }

    protected function formatDatesToRange(Carbon $startDate, Carbon $endDate): string
    {
        return '[' .
            $startDate->format('Y-m-d 00:00:00+00') . ',' .
            $endDate->format('Y-m-d 00:00:00+00') .
            ')';
    }

    abstract protected function model();

    /**
     * @throws RepositoryException
     */
    protected function query(): Builder
    {
        $query = $this->makeModel();

        foreach ($this->orderBy as $column => $direction) {
            $query->orderBy($column, $direction);
        }

        return $query;
    }
}
