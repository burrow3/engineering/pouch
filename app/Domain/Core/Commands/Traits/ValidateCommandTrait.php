<?php

declare(strict_types=1);

namespace App\Domain\Core\Commands\Traits;

trait ValidateCommandTrait
{
    private bool $disableValidation = false;

    abstract public function getValidator(): string;

    public function getValidationDetails(): array
    {
        $details = $this->details;

        if (\property_exists($this, 'id')) {
            $details['id'] = $this->id;
        }

        return $details;
    }

    public function setValidationDisabled(bool $disableValidation): self
    {
        $this->disableValidation = $disableValidation;

        return $this;
    }

    public function getValidationDisabled(): bool
    {
        return $this->disableValidation;
    }
}
