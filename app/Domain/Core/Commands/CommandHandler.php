<?php

declare(strict_types=1);

namespace App\Domain\Core\Commands;

use Illuminate\Contracts\Auth\Guard;
use App\Domain\Core\Events\DispatchesEvents;
use Ramsey\Uuid\Uuid;

abstract class CommandHandler
{
    use DispatchesEvents;

    /**
     * @var Guard
     */
    protected $auth;

    /**
     * @var array
     */
    protected $details = [];

    protected function getEntityId(): string
    {
        if (isset($this->details['id']) && \is_string($this->details['id']) && Uuid::isValid($this->details['id'])) {
            return (string) $this->details['id'];
        }

        return Uuid::uuid4()->toString();
    }
}
