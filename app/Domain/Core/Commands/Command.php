<?php

declare(strict_types=1);

namespace App\Domain\Core\Commands;

abstract class Command
{
    /**
     * Validation details for the command
     *
     * @var array
     */
    public $details = [];

    /**
     * Array of named ACLs required for the command to run
     *
     * @var array
     */
    protected $ACLs = [];

    /**
     * @var bool
     */
    private $dryRun = false;

    public function getACLs()
    {
        return $this->ACLs;
    }

    public function dryRun(): self
    {
        $this->dryRun = true;

        return $this;
    }

    public function wetRun(): self
    {
        $this->dryRun = false;

        return $this;
    }

    public function isDryRun(): bool
    {
        return $this->dryRun;
    }

    public function getDetails(): array
    {
        return $this->details;
    }
}
