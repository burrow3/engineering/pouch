<?php

declare(strict_types=1);

namespace App\Domain\Core\Events;

use App\Domain\User\User;

abstract class Event
{
    /**
     * The tenant this event was fired on.
     *
     * This is only set after an event is unserialized and not when it is created.
     * This needs to stay docblocked until we migrate the rest of the typings.
     *
     * @var string $tenant
     */
    protected $tenant = '';

    /**
     * The id of the user who was logged in when this event was fired.
     *
     * This is only set after an event is unserialized and not when it is created.
     */
    protected string $actioningUserId = '';

    /**
     * The id of the event log that this event is related to.
     * Only set if `setEventLogId()` is called.
     */
    protected ?string $eventLogId = null;

    public function __sleep()
    {
        $this->tenant = config('pouch.tenant', '');

        $this->actioningUserId = '';
        if (!\in_array($this->tenant, ['', 'public'])) {
            $this->actioningUserId = app()->auth->id() ?? '';
        }

        return \array_keys(\get_object_vars($this));
    }

    public function __wakeup()
    {
        if (!empty($this->actioningUserId)) {
            if (!empty(User::find($this->actioningUserId))) {
                app()->auth->loginUsingId($this->actioningUserId);
            }
        }
    }

    public function setEventLogId(string $id): self
    {
        $this->eventLogId = $id;

        return $this;
    }

    public function getEventLogId(): ?string
    {
        return $this->eventLogId;
    }

    /**
     * Returns null when this event is not unserialised
     */
    public function getActioningUserId(): ?string
    {
        return $this->actioningUserId;
    }

    public function shouldRecordInEventLogTable(): bool
    {
        return true;
    }
}
