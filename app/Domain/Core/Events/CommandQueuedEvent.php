<?php

declare(strict_types=1);

namespace App\Domain\Core\Events;

class CommandQueuedEvent extends Event
{
    public string $commandClass;
    public array $commandParams;

    public function __construct(string $commandClass, array $commandParams)
    {
        $this->commandClass = $commandClass;
        $this->commandParams = $commandParams;
    }
}
