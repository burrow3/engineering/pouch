<?php

declare(strict_types=1);

namespace App\Domain\Core\Events\Contracts;

interface Dispatcher
{
    /**
     * Dispatch all raised events.
     *
     * @param array $events
     */
    public function dispatch(array $events);
}
