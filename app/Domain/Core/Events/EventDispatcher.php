<?php

declare(strict_types=1);

namespace App\Domain\Core\Events;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Events\Dispatcher;
use App\Tactician\Traits\DispatchesCommands;
use App\Domain\Core\Events\Contracts\Dispatcher as DispatcherInterface;
use App\Domain\EventLog\EloquentEventLog;
use App\Domain\EventLog\EventLogRepository;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

class EventDispatcher implements DispatcherInterface
{
    use DispatchesCommands {
        dispatch as dispatchCommand;
    }
    protected Dispatcher $event;
    protected LoggerInterface $log;
    protected Guard $auth;
    private EventLogRepository $eventLogRepository;

    public function __construct(
        EventLogRepository $eventLogRepository,
        Dispatcher $event,
        LoggerInterface $log,
        Guard $auth
    ) {
        $this->eventLogRepository = $eventLogRepository;
        $this->event = $event;
        $this->log = $log;
        $this->auth = $auth;
    }

    public function dispatch(array $events)
    {
        foreach ($events as $event) {
            $eventName = $this->getEventName($event);

            $this->recordEvent($eventName, $event);

            try {
                $this->event->dispatch($eventName, $event);
            } catch (\Exception $e) {
                $this->log
                    ->error(
                        "An exception occurred when dispatching the event {$eventName}",
                        [
                            'exception' => $e,
                            'exceptionClass' => get_class($e),
                            'exceptionMessage' => $e->getMessage(),
                            'eventName' => $eventName,
                            'event' => $event,
                        ]
                    );
            }

            $this->log->info("{$eventName} was fired.");
        }
    }

    protected function getEventName(Event $event): string
    {
        return str_replace('\\', '.', get_class($event));
    }

    private function recordEvent(string $eventName, Event $event): void
    {
        if (!$event->shouldRecordInEventLogTable()) {
            return;
        }

        $model = new EloquentEventLog();

        $model->event_name = $eventName;
        $model->event = $event;
        $model->id = Uuid::uuid4()->toString();

        $this->eventLogRepository->save($model);

        $event->setEventLogId($model->id);
    }
}
