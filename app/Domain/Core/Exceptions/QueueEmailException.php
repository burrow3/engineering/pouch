<?php

declare(strict_types=1);

namespace App\Domain\Core\Exceptions;

use Exception;

class QueueEmailException extends Exception
{
}
