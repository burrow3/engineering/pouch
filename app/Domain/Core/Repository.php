<?php

declare(strict_types=1);

namespace App\Domain\Core;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use App\Domain\Core\Database\Eloquent\Model;

interface Repository
{
    /**
     * @param array $columns
     *
     * @return Collection
     */
    public function all(array $columns = ['*']);

    public function create(array $data = []);

    public function delete($id);

    public function deleteWhereIn(string $field, array $value): int;

    public function eagerLoad($relations): self;

    public function find(string $id, array $columns = ['*']);

    public function findOrFail(string $id, array $columns = ['*']);

    public function getFilterable(): array;

    public function getOrderBy(): array;

    public function makeModel(): Builder;

    public function orderBy($columns = []): self;

    public function save(Model $model): bool;

    public function saveMinor(Model $model, array $options = []): bool;

    public function update(string $id, array $data = []): void;

    public function where($field, $operator, $value, $columns = ['*']);

    public function whereIn(string $field, array $values, array $columns = ['*']): Collection;

    public function with(array $array);
}
