<?php

declare(strict_types=1);

namespace App\Domain\Core\Validators;

use Illuminate\Validation\Factory;
use Illuminate\Validation\Validator;
use App\Exceptions\ValidationException;

abstract class AbstractValidator
{
    /**
     * @var Factory
     */
    protected $validator;

    /**
     * @var array
     */
    protected $details = [];

    public function __construct(Factory $validator)
    {
        $this->validator = $validator;
    }

    public function validate(array $details = []): bool
    {
        $this->details = $details;
        $rules = $this->getRules();
        $messages = $this->getMessages();

        $validator = $this->validator->make(
            $this->details,
            $rules,
            $messages
        );

        $validator = $this->appendValidatorRules($validator);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return true;
    }

    abstract protected function getRules(): array;

    protected function getMessages(): array
    {
        return [];
    }

    protected function appendValidatorRules(Validator $validator): Validator
    {
        return $validator;
    }
}
