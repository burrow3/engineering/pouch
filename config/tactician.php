<?php

declare(strict_types=1);

return [
    'locator' => App\Tactician\Locator\PouchLocator::class,
    'extractor' => League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor::class,
    'inflector' => League\Tactician\Handler\MethodNameInflector\HandleInflector::class,
    'bus' => App\Tactician\Bus::class,
];
