FROM php:8.2-fpm-alpine

COPY . /var/www/html/

WORKDIR /var/www/html/

RUN apk update && apk add libpq-dev

RUN docker-php-ext-install pdo pdo_pgsql pgsql

RUN curl -sS https://getcomposer.org/installer | php \
    && php composer.phar install

CMD ["php-fpm"]