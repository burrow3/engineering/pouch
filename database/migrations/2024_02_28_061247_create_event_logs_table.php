<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Query\Expression;

class CreateEventLogsTable extends Migration
{
    public function up(): void
    {
        Schema::create('event_logs', static function (Blueprint $table): void {
            $table->uuid('id')->primary()->default(new Expression('public.gen_random_uuid()'));

            $table->text('event_name');
            $table->jsonb('event');

            $table->timestampTz('created_at');
            $table->timestampTz('updated_at');
            $table->timestampTz('deleted_at')->nullable();
        });
    }
}
