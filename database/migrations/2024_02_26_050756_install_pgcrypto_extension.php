<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;

class InstallPgcryptoExtension extends Migration
{
    protected array $schemas = [
        'public',
    ];

    public function up(): void
    {
        DB::statement('CREATE EXTENSION IF NOT EXISTS pgcrypto');
    }

    public function down(): void
    {
        DB::statement('DROP EXTENSION IF EXISTS pgcrypto');
    }
}
